import { IsNotEmpty, IsPositive } from 'class-validator';

class CreateOrderItemDto {
  @IsNotEmpty()
  productId: number;

  @IsNotEmpty()
  @IsPositive()
  amount: number;
}
export class CreateOrderDto {
  @IsNotEmpty()
  customerId: number;
  orderItems: CreateOrderItemDto[];
}
